# Build
FROM node:14.19.1-alpine as build

RUN apk add --no-cache yarn

WORKDIR /usr/src/app
COPY . .
RUN yarn install --frozen-lockfile
RUN yarn build


# App
FROM nginx:1.21.6-alpine as app

COPY --from=build /usr/src/app/build /var/www/localhost/htdocs

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/localhost.conf /etc/nginx/conf.d/localhost.conf

